package main

import "behavioral-pattern-chain-of-responsibility/pkg"

func main() {
	cashier := &pkg.Cashier{}

	medical := &pkg.Medical{}
	medical.SetNext(cashier)

	doctor := &pkg.Doctor{}
	doctor.SetNext(medical)

	reception := &pkg.Reception{}
	reception.SetNext(doctor)

	patient := &pkg.Patient{Name: "Thang"}
	reception.Execute(patient)
}