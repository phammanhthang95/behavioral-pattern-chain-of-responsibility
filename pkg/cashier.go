package pkg

import "fmt"

type Cashier struct {
	next Department
}

func (r *Cashier) Execute(p *Patient) {
	if p.isRegistered {
		fmt.Println("Cashier has already done")
		return
	}
	fmt.Println("Cashier is checking patient")
	p.isPaid = true
}

func (r *Cashier) SetNext(next Department) {
	r.next = next
}