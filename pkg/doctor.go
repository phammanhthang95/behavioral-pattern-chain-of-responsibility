package pkg

import "fmt"

type Doctor struct {
	next Department
}

func (r *Doctor) Execute(p *Patient) {
	if p.isRegistered {
		fmt.Println("Patient doctor has already done")
		r.next.Execute(p)
		return
	}
	fmt.Println("Doctor is checking patient")
	p.isDoctorChecked = true
	r.next.Execute(p)
}

func (r *Doctor) SetNext(next Department) {
	r.next = next
}