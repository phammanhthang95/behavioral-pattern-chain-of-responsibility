package pkg

import "fmt"

type Medical struct {
	next Department
}

func (r *Medical) Execute(p *Patient) {
	if p.isRegistered {
		fmt.Println("Medical has already done")
		r.next.Execute(p)
		return
	}
	fmt.Println("Medical is checking patient")
	p.isMedicineProvided = true
	r.next.Execute(p)
}

func (r *Medical) SetNext(next Department) {
	r.next = next
}